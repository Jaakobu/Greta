<?php
// Debug mode
define('DEBUG', true);

// Database informations
define('DB_HOST', 'localhost');
define('DB_NAME', 'ge_movies');
define('DB_USER', 'root');
define('DB_PASSWORD', '');

// Path
define('BASEPATH', '/php/structure');
define('ROOT', dirname(__DIR__));
define('CONTROLLERS', ROOT . '/controllers/');
define('PAGES', ROOT . '/views/pages/');
