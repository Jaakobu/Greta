<?php
$menus = [
	'index.php' => 'Home',
	'building.php' => 'MAM',
	'about_us.php' => 'Equipe',
	'gallery.php' => 'Fonctionnement',
	'our-blog.php' => 'Besoin Enfants',
    'our-blog.php' => 'Activités',
    'contact.php' => 'Contact'
    
];

$social = [
	[
		'name' => 'Facebook',
		'icon' => 'facebook.png',
		'url'  => 'http://www.facebook.com'
	],
	[
		'name' => 'Pinterest',
		'icon' => 'pinterest.png',
		'url'  => 'http://www.facebook.com'
	],
	[
		'name' => 'Instagram',
		'icon' => 'instagram.png',
		'url'  => 'http://www.facebook.com'
	]
];


$properties = [
	[
		'picture'   => 'assets/images/property1.jpg',
		'adress'  => '1450 Cloudcroft Drop',
		'city' => 'Illinois / Chicago',
		'price' => 250000,
		'url' => 'index.php',
		'services' => [
			[
				'label' => 'bedroom',
				'number' => 1
			],
			[
				'label' => 'bedroom',
				'number' => 2
			],
			[
				'label' => 'bedroom',
				'number' => 2
			]
		]
	],
	[
		'picture'   => 'assets/images/property2.jpg',
		'adress'  => '140 Small Village',
		'city' => 'Missouri / Kansas City',
		'price' => 200000,
		'url' => 'index.php',
		'services' => [
			[
				'label' => 'bedroom',
				'number' => 2
			],
			[
				'label' => 'bedroom',
				'number' => 2
			],
			[
				'label' => 'bedroom',
				'number' => 2
			]
		]
	],
	[
		'picture'   => 'assets/images/property3.jpg',
		'adress'  => '1250 Lake House',
		'city' => 'Colorado / Denver',
		'price' => 339000,
		'url' => 'index.php',
		'services' => [
			[
				'label' => 'bedroom',
				'number' => 2
			],
			[
				'label' => 'bedroom',
				'number' => 2
			],
			[
				'label' => 'bedroom',
				'number' => 2
			]
		]
	],
	[
		'picture'   => 'assets/images/property4.jpg',
		'adress'  => '1450 Cloudcroft Drop',
		'city' => 'Illinois / Chicago',
		'price' => 250000,
		'url' => 'index.php',
		'services' => [
			[
				'label' => 'bedroom',
				'number' => 2
			],
			[
				'label' => 'bedroom',
				'number' => 2
			],
			[
				'label' => 'bedroom',
				'number' => 2
			]
		]
	],
	[
		'picture'   => 'assets/images/property5.jpg',
		'adress'  => '140 Small Village',
		'city' => 'Missouri / Kansas City',
		'price' => 200000,
		'url' => 'index.php',
		'services' => [
			[
				'label' => 'bedroom',
				'number' => 2
			],
			[
				'label' => 'bedroom',
				'number' => 2
			],
			[
				'label' => 'bedroom',
				'number' => 2
			]
		]
	],
	[
		'picture'   => 'assets/images/property6.jpg',
		'adress'  => '1250 Lake House',
		'city' => 'Colorado / Denver',
		'price' => 339000,
		'url' => 'index.php',
		'services' => [
			[
				'label' => 'bedroom',
				'number' => 2
			],
			[
				'label' => 'bedroom',
				'number' => 2
			],
			[
				'label' => 'bedroom',
				'number' => 2
			]
		]
	]
];
