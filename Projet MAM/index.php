<?php

require_once('includes/data.php');


?>

<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Admin</title>
	<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
</head>
<header>
	<nav class="navbar navbar-light navbar-expand-lg mb-5" style="background-color: #fff;">
		<div class="container">
			<a href="" class="navbar-brand">Home</a>
			<div class="collapse navbar-collapse">
				<ul class="navbar-nav">
					<li class="nav-item">
						<a class="nav-link" href="" title="Notre MAM">Notre MAM</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="" title="Fonctionnement">Fonctionnement</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="" title="L'Equipe">L'Equipe</a>
					</li>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="" title="Besoins de l'enfant">Besoins de l'enfant</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="" title="Activités">Activités</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="" title="Contact">Contact</a>
					</li>
				</ul>
			</div>
			<div class="my-2 my-lg-0">
				<a href="" class="btn btn-primary" title="Connexion">Connexion</a>
			</div>
		</div>
	</nav>
</header>
<div class="container">
	<h2>La MAM</h2>
	<p>Haurren Bihotza est une association dédiée à la petite enfance. Elle met à
		disposition dans la commune d'Urrugne un lieu conçu pour les jeunes enfants de 2 mois à 6 ans.
		La Maison d'Assistantes Maternelles se situe dans un écrin de verdure et de douceur.
		Haurren Bihotza a sélectionné des équipements de qualité éco-certifiés ainsi que des produits bio
		dans les assiettes comme pour l'hygiène. Les goûters sont réalisés sur place chaque jour avec des produits locaux ou bio.
		L'équipe d'assistantes maternelles est expérimentée et bienveillante.
		Les assistantes maternelles sont agréées par le Conseil Départemental des Pyrénées Atlantiques et sont en partenariat constant avec les équipes de la PMI.
		Les parents sont les employeurs et perçoivent le complément de mode de garde avec le soutien de la CAF en fonction de leur revenu,
		ou de la MSA, et éventuellement de votre employeur. Ils peuvent également bénéficier d'un crédit d'impôts.
	</p>
</div>
<div>
	<img src="" alt="">
	<p>7h30 à 19h</p>
	<p>lundi à vendredi</p>
	<p>des horaires adaptés à votre vie professionnelle et familiale</p>
</div>
<div>
	<img src="" alt="">
	<p>3 professionnelles de la petite enfance qualifiées initient en douceur vos enfants à la vie en collectivité</p>
</div>
<div>
	<img src="" alt="">
	<p>12 enfants de 2 mois à 6 ans s'épanouissent et grandissent dans la chaleureuse maison en pleine nature</p>
</div>
<div>
	<img src="" alt="">
	<p>x m² dont : salle d'éveil, espace "cocoon" pour les bébés, 2 chambres, salles d'éveil avec espace de motricité et d'activités manuelles, sanitaires enfants à chaque étage jardin et parking</p>
</div>



<div>
	<p>Notre philosophie</p>
	<p>La Maison Haurren Bihotza permet aux jeunes enfants d'évoluer en toute
		sécurité dans un cadre verdoyant. Les professionnelles, attentives à chaque enfant, proposent des activités selon les envies et les capacités de chacun afin de leur permettre d'acquérir de l'autonomie et de prendre confiance en eux.
		Chaque enfant a son assistante maternelle de référence qui lui prodigue les soins et le guide dans son quotidien à la MAM. Les activités sont très riches et variées s'appuyant sur la pédagogie de Maria Montessori, Jean Epstein et Marshall B. Rosenberg.
		Ils pourront tour à tour découvrir les lettres, développer leur motricité fine, le langage, assister à des racontées en bibliothèque, découvrir les goûts, la communication pré-verbale, réaliser des collations saines ou visiter les animaux (autour de la MAM on peut trouver lapins, poules, chevaux, vaches et oies !), participer au potager, jouer au parc situé à quelques pas...</p>
</div>


<div>
	<div>
		<h2>Une Équipe dévouée</h2>
	</div>
	<div>
		<img src="" alt="">
	</div>
	<div>
		<h3>Jessica JARA JIMENEZ</h3>
		<h4>Assistante maternelle agréée</h4>
		<p>Titulaire du CAP petite enfance ... </p>
	</div>
	<div>
		<img src="" alt="">
	</div>
	<div>
		<h3>Maeva Mateos</h3>
		<h4>Assistante maternelle agréée</h4>
		<p>Titulaire du CAP petite enfance ... </p>
	</div>
	<div>
		<img src="" alt="">
	</div>
	<div>
		<h3>Lucie BAILLET</h3>
		<h4>Assistante maternelle agréée</h4>
		<p>Titulaire du titre Educateur de Jeunes Enfants... </p>
	</div>
</div>














<div>
	<div>
		<h2>Venez nous rendre visite ou laissez-nous un petit mot</h2>
	</div>
	<div>
		<p>L'équipe est à votre écoute pour vous renseigner et réaliser selon vos besoins un devis pour l'accueil de votre enfant.</p>
	</div>
	<div>
		<ul>
			<li>Maison Hurren Bihotza</li>
			<li>11 chemin Herriko Bihotza - 64210 Ahetze </li>
			<li>mammlj@gmail.com</li>
			<li>05 05 05 05 05</li>
		</ul>
	</div>
	<div>
		<table width="500" border="0" align="center" cellpadding="0" cellspacing="0">
			<form action="envoi.php" method="post" enctype="application/x-www-form-urlencoded" name="formulaire">
				<tr>
					<td>
						<div align="left">Votre nom :</div>
					</td>
					<td colspan="2"><input type="text" name="nom" size="45" maxlength="100"></td>
				</tr>
				<tr>
					<td width="17%">
						<div align="left">Votre mail :</div>
					</td>
					<td colspan="2"><input type="text" name="mail" size="45" maxlength="100"></td>
				</tr>
				<tr>
					<td width="17%">
						<div align="left">Téléphone :</div>
					</td>
					<td colspan="2"><input type="text" name="telephone" size="45" maxlength="100"></td>
				</tr>
				<tr>
					<td>
						<div align="left">Sujet : </div>
					</td>
					<td colspan="2"><input type="text" name="objet" size="45" maxlength="120"></td>
				</tr>
				<tr>
					<td>
						<div align="left">Message : </div>
					</td>
					<td colspan="2"><textarea name="message" cols="50" rows="10"></textarea></td>
				</tr>
				<tr>
					<td></td>
					<td width="42%">
						<center>
							<input type="reset" name="Submit" value="Réinitialiser le formulaire">
						</center>
					</td>
					<td width="41%">
						<center>
							<input type="submit" name="Submit" value="Envoyer">
						</center>
					</td>
				</tr>
			</form>
		</table>

		</form>
	</div>
</div>



<script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
</body>






</html>