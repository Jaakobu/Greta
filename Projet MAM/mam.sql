CREATE DATABASE mam;
CREATE USER IF NOT EXISTS mam IDENTIFIED BY 'Mammlj64';
GRANT SELECT, INSERT, UPDATE, DELETE ON mam.* TO 'mam'@'localhost' IDENTIFIED BY 'Mammlj64';

USE mam;

CREATE TABLE user (
    id 				INTEGER AUTO_INCREMENT NOT NULL,
    name 			VARCHAR(255) NOT NULL,
    surname			VARCHAR(255) NOT NULL,
    email 			VARCHAR(255) NOT NULL,
    password			VARCHAR(255) NOT NULL,
    role			BOOLEAN NOT NULL,
    date_creation 		TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    created_by 			INTEGER,
    date_modification 		TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_by			INTEGER,
   PRIMARY KEY(id),
   CONSTRAINT fk_created_by
		FOREIGN KEY (created_by) REFERENCES user(id),
    CONSTRAINT fk_modified_by
		FOREIGN KEY (modified_by) REFERENCES user(id)
)ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE media (
    id				INTEGER AUTO_INCREMENT NOT NULL,
    media_name 			VARCHAR(255) NOT NULL,
    path_media			VARCHAR(255) NOT NULL,
   PRIMARY KEY(id)
)ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE category (
    id				INTEGER AUTO_INCREMENT NOT NULL,
    name 			VARCHAR(255) NOT NULL,

   PRIMARY KEY(id)
)ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE article (
    id 				INTEGER AUTO_INCREMENT NOT NULL,
    title 			VARCHAR(255) NOT NULL,
    description			VARCHAR(255) NOT NULL,
    date_creation 		TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    user_id			INTEGER,
    media			VARCHAR(255) NOT NULL,
    category_id			INTEGER,
    date_modification 		TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_by			INTEGER,
   PRIMARY KEY(id),
   CONSTRAINT fk_user_id
		FOREIGN KEY (user_id) REFERENCES user(id),
    CONSTRAINT fk_zoo_id
		FOREIGN KEY (modified_by) REFERENCES user(id),
CONSTRAINT fk_category_id
		FOREIGN KEY (category_id) REFERENCES category(id)
)ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE article_media (
    article_id			INTEGER,
    media_id 			INTEGER,
   PRIMARY KEY(article_id, media_id),
   CONSTRAINT fk_article
		FOREIGN KEY (article_id) REFERENCES article(id),
    CONSTRAINT fk_media
		FOREIGN KEY (media_id) REFERENCES media(id)
)ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE children (
    id 				INTEGER AUTO_INCREMENT NOT NULL,
    name 			VARCHAR(255) NOT NULL,
    surname			VARCHAR(255) NOT NULL,
    email 			VARCHAR(255) NOT NULL,
    date_of_birth		VARCHAR(255) NOT NULL,
    comment			BOOLEAN NOT NULL,
    user_id 			INTEGER,
    date_creation 		TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    created_by 			INTEGER,
    date_modification 	        TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_by			INTEGER,
   PRIMARY KEY(id),
   CONSTRAINT fk_toto_id
		FOREIGN KEY (user_id) REFERENCES user(id),
CONSTRAINT fk_tote_id
		FOREIGN KEY (modified_by) REFERENCES user(id)
)ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;

CREATE TABLE calendar (
    id 			INTEGER AUTO_INCREMENT NOT NULL,
    date 			VARCHAR(255) NOT NULL,
    user_id 			INTEGER,
    children_id 		INTEGER,
    date_creation 		TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    created_by 			INTEGER,
    date_modification 		TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    modified_by			INTEGER,
   PRIMARY KEY(id),
   CONSTRAINT fk_children_id
		FOREIGN KEY (children_id) REFERENCES children(id),
    CONSTRAINT fk_tata_id
		FOREIGN KEY (user_id) REFERENCES user(id),
    CONSTRAINT fk_tate_id
		FOREIGN KEY (modified_by) REFERENCES user(id)
)ENGINE=InnoDB CHARACTER SET utf8 COLLATE utf8_general_ci;
